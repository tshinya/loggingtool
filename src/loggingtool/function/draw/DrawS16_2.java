package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S16_2;
import loggingtool.ui.MainFrame;

public class DrawS16_2 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S16_2.Iterator(), DefaultData.S16_2);
	}

}

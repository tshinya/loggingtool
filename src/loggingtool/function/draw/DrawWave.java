package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.Wave;
import loggingtool.ui.MainFrame;

public class DrawWave implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(Wave.Iterator(), DefaultData.WAVE);
	}

}

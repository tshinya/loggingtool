package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S16_3;
import loggingtool.ui.MainFrame;

public class DrawS16_3 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S16_3.Iterator(), DefaultData.S16_3);
	}

}

package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.MotorC;
import loggingtool.ui.MainFrame;

public class DrawMotorC implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(MotorC.Iterator(), DefaultData.MOTOR_C);
	}

}

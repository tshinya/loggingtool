package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S8_2;
import loggingtool.ui.MainFrame;

public class DrawS8_2 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S8_2.Iterator(), DefaultData.S8_2);
	}

}

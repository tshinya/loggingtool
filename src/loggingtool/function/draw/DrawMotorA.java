package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.MotorA;
import loggingtool.ui.MainFrame;

public class DrawMotorA implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(MotorA.Iterator(), DefaultData.MOTOR_A);
	}

}

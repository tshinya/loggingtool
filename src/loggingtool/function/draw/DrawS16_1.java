package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S16_1;
import loggingtool.ui.MainFrame;

public class DrawS16_1 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S16_1.Iterator(), DefaultData.S16_1);
	}

}

package loggingtool.function.draw;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import javax.swing.table.JTableHeader;

public class DrawActionAdapter extends MouseAdapter {
	
	private JTable table;
	private DrawChartMap map;
	
	public DrawActionAdapter(JTable _table) {
		table = _table;
		map = new DrawChartMap();
	}
	
	@Override
	public void mousePressed(MouseEvent e) {
		super.mousePressed(e);
		JTableHeader header = table.getTableHeader();
		int column = header.columnAtPoint(e.getPoint());
		String key = table.getColumnName(column);
		map.drawChart(key);
	}

}
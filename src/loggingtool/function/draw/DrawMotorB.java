package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.MotorB;
import loggingtool.ui.MainFrame;

public class DrawMotorB implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(MotorB.Iterator(), DefaultData.MOTOR_B);
	}

}

package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S16_4;
import loggingtool.ui.MainFrame;

public class DrawS16_4 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S16_4.Iterator(), DefaultData.S16_4);
	}

}

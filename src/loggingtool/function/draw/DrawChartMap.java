package loggingtool.function.draw;

import java.util.HashMap;
import loggingtool.data.DefaultData;

public class DrawChartMap {
	
	private HashMap<String, DrawChartIFace> map;
	
	public DrawChartMap() {
		map = new HashMap<>();
		map.put(DefaultData.S8_1, new DrawS8_1());
		map.put(DefaultData.S8_2, new DrawS8_2());
		map.put(DefaultData.LIGHT, new DrawLight());
		map.put(DefaultData.MOTOR_A, new DrawMotorA());
		map.put(DefaultData.MOTOR_B, new DrawMotorB());
		map.put(DefaultData.MOTOR_C, new DrawMotorC());
		map.put(DefaultData.S16_1, new DrawS16_1());
		map.put(DefaultData.S16_2, new DrawS16_2());
		map.put(DefaultData.S16_3, new DrawS16_3());
		map.put(DefaultData.S16_4, new DrawS16_4());
		map.put(DefaultData.WAVE, new DrawWave());
	}
	
	public void drawChart(String key) {
		if (!map.containsKey(key)) {
			return ;
		}
		DrawChartIFace drawChartIFace = map.get(key);
		drawChartIFace.drawChart();
	}

}
package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.Light;
import loggingtool.ui.MainFrame;

public class DrawLight implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(Light.Iterator(), DefaultData.LIGHT);
	}

}

package loggingtool.function.draw;

import loggingtool.data.DefaultData;
import loggingtool.data.receive.S8_1;
import loggingtool.ui.MainFrame;

public class DrawS8_1 implements DrawChartIFace {

	@Override
	public void drawChart() {
		MainFrame.updateChart(S8_1.Iterator(), DefaultData.S8_1);
	}

}

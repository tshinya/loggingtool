package loggingtool.function.connection;

import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import loggingtool.data.address.Address;
import loggingtool.function.connection.queue.DeQueue;
import loggingtool.function.connection.queue.EnQueue;
import loggingtool.ui.log.LogJTable;

public class BlueToothConnection {

	private static StreamConnection streamConnection;
	private static boolean connectFlg;
	private static LogJTable logJTable;
	
	private BlueToothConnection() {
		
	}
	
	public static void setLogJTable(LogJTable _logJTable) {
		logJTable = _logJTable;
	}
	
	static boolean connect() {
		connectFlg = false;
		try {
			streamConnection = (StreamConnection) Connector.open(Address.get());
			connectFlg = true;
			System.out.println("Connect");
		} catch (IOException e) {
			System.out.println("Error : BlueToothConnection.connect()");
		}
		return connectFlg;
	}
	
	static boolean send(String sendData) {
		try {
			Send sendingData = new Send(streamConnection.openOutputStream());
			return sendingData.send(sendData);
		} catch (IOException e) {
			System.out.println("Error : BlueToothConnection.send()");
			return false;
		}
	}
	
	public static void receive() {
		try {
			EnQueue receive = new EnQueue(streamConnection.openDataInputStream());
			new Thread(receive).start();
			new Thread(new DeQueue(logJTable)).start();
		} catch (IOException e) {
			System.out.println("Error : BlueToothConnection.receive()");
		}
	}
	
	static void close() {
		try {
			streamConnection.close();
			connectFlg = false;
			System.out.println("Close");
		} catch (IOException e) {
			System.out.println("Error : BlueToothConnection.close()");
		}
	}
	
	public static boolean isConnect() {
		return connectFlg;
	}
}
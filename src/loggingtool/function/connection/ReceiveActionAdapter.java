package loggingtool.function.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import loggingtool.data.DefaultData;
import loggingtool.ui.connect.IOManager;
import loggingtool.ui.connect.receive.ReceiveJBtn;
import loggingtool.ui.connect.send.SendJBtn;

public class ReceiveActionAdapter implements ActionListener {
	
	private ReceiveJBtn receiveJBtn;
	private SendJBtn sendJBtn;
	
	public ReceiveActionAdapter() {
		receiveJBtn = IOManager.getReceiveJBtn();
		sendJBtn = IOManager.getSendJBtn();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (sendJBtn.isEnabled()) {
			BlueToothConnection.send(DefaultData.NO_DATA);
		}
		BlueToothConnection.receive();
		receiveJBtn.changeSelectable(false);
		sendJBtn.changeSelectable(false);
	}

}
package loggingtool.function.connection.queue;

import java.util.ArrayList;

public class Queue {

	private static ArrayList<String> list = new ArrayList<>();
	
	private Queue() {
		
	}
	
	synchronized static void enQueue(String s) {
		list.add(s);
	}
	
	synchronized static String deQueue() {
		if (isEnpty()) {
			return "";
		}
		String s = list.get(0);
		list.remove(0);
		return s;
	}
	
	synchronized static boolean isEnpty() {
		if (list.size() == 0) {
			return true;
		}
		return false;
	}
	
	synchronized static void clear() {
		list.clear();
	}
}
package loggingtool.function.connection.queue;

import loggingtool.function.connection.BlueToothConnection;
import loggingtool.ui.log.LogJTable;

public class DeQueue implements Runnable {
	
	private LogJTable logJTable;
	
	public DeQueue(LogJTable _logJTable) {
		logJTable = _logJTable;
	}

	@Override
	public void run() {
		while (BlueToothConnection.isConnect()) {
			if (!Queue.isEnpty()) {
				String s = Queue.deQueue();
				String[] row = s.split(",");
				logJTable.appendLog(row);
			}	
		}
	}

}

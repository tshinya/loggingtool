package loggingtool.function.connection.queue;

import java.io.DataInputStream;
import loggingtool.function.connection.BlueToothConnection;
import loggingtool.function.connection.Receve;
public class EnQueue implements Runnable {

	private Receve receivingData;
	
	public EnQueue(DataInputStream dataInputStream) {
		receivingData = new Receve(dataInputStream);
	}
	
	@Override
	public void run() {
		while (BlueToothConnection.isConnect()) {
			String str = receivingData.receive();
			if (!str.equals("")) {
				Queue.enQueue(str);
			}
		}
	}

}

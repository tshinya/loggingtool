package loggingtool.function.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import loggingtool.ui.connect.IOManager;
import loggingtool.ui.connect.address.ConnectJBtn;

public class ConnectActionAdapter implements ActionListener {
	
	private ConnectJBtn connectJBtn;
	
	public ConnectActionAdapter(ConnectJBtn _connectJBtn) {
		connectJBtn = _connectJBtn;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (BlueToothConnection.isConnect()) {
			disconnect();
			return ;
		}	
		connect();
	}
	
	private void connect() {
		BlueToothConnection.connect();
		connectJBtn.connect();
		IOManager.getSendJBtn().changeSelectable(true);
		IOManager.getReceiveJBtn().changeSelectable(true);
	}
	
	private void disconnect() {
		BlueToothConnection.close();
		connectJBtn.disConnect();
		IOManager.getSendJBtn().changeSelectable(false);
		IOManager.getReceiveJBtn().changeSelectable(false);
	}

}
package loggingtool.function.connection;

import java.io.DataInputStream;
import java.io.IOException;
import loggingtool.data.receive.Convertor;
import loggingtool.data.receive.Light;
import loggingtool.data.receive.MotorA;
import loggingtool.data.receive.MotorB;
import loggingtool.data.receive.MotorC;
import loggingtool.data.receive.S16_1;
import loggingtool.data.receive.S16_2;
import loggingtool.data.receive.S16_3;
import loggingtool.data.receive.S16_4;
import loggingtool.data.receive.S8_1;
import loggingtool.data.receive.S8_2;
import loggingtool.data.receive.Time;
import loggingtool.data.receive.Wave;

public class Receve {
	
	private DataInputStream dataInputStream;
	
	public Receve(DataInputStream _dataInputStream) {
		dataInputStream = _dataInputStream;
		Time.clear();
		Light.clear();
		S8_1.clear();
		S8_2.clear();
		MotorA.clear();
		MotorB.clear();
		MotorC.clear();
		S16_1.clear();
		S16_2.clear();
		S16_3.clear();
		S16_4.clear();
		Wave.clear();
	}
	
	public String receive() {
		byte[] buf = new byte[32];
		try {
			while (dataInputStream.available() == 0) { }
			dataInputStream.readFully(buf, 0, buf.length);
		} catch (Exception e) {
			close();
			return "";
		}

		int time = Convertor.toUInt32(buf, 0);
		int light = Convertor.toInt16(buf, 6);
		int s8_1 = buf[4];
		int s8_2 = buf[5];
		int motorA = Convertor.toInt32(buf, 8);
		int motorB = Convertor.toInt32(buf, 12);
		int motorC = Convertor.toInt32(buf, 16);
		int s16_1 = Convertor.toInt16(buf, 20);
		int s16_2 = Convertor.toInt16(buf, 22);
		int s16_3 = Convertor.toInt16(buf, 24);
		int s16_4 = Convertor.toInt16(buf, 26);
		int wave = Convertor.toInt32(buf, 28);
		
		Time.add(time);
		Light.add(light);
		S8_1.add(s8_1);
		S8_2.add(s8_2);
		MotorA.add(motorA);
		MotorB.add(motorB);
		MotorC.add(motorC);
		S16_1.add(s16_1);
		S16_2.add(s16_2);
		S16_3.add(s16_3);
		S16_4.add(s16_4);
		Wave.add(wave);
		
		String newLine = System.getProperty("line.separator");
		String str = time + ", " + s8_1 + ", " + s8_2 + ", " + light + ", " + motorA + ", " + motorB + ", " + motorC + ", " + s16_1 + ", " + s16_2 + ", " + s16_3 + ", " + s16_3 + ", " + s16_4 + ", " + wave + newLine;
		return str;
	}
	
	public void close() {
		try {
			dataInputStream.close();
		} catch (IOException e) {
			System.out.println("Error : DataInputStream.close");
		}
	}
}
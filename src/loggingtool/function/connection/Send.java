package loggingtool.function.connection;

import java.io.IOException;
import java.io.OutputStream;

public class Send {
	
	private OutputStream outputStream;
	
	Send(OutputStream _outputStream) {
		outputStream = _outputStream;
	}

	public boolean send(String sendData) {
		
		if (!BlueToothConnection.isConnect()) {
			return false;
		}
		
		try {
			outputStream.write(sendData.getBytes(), 0, sendData.length());
			outputStream.flush();
		} catch (IOException e) {
			close();
			BlueToothConnection.close();
			return false;
		}
		return true;
	}
	
	public void close() {
		try {
			outputStream.close();
		} catch (IOException e) {
			System.out.println("Error ; OutputStream.close");
		}
	}
}
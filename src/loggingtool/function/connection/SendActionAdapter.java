package loggingtool.function.connection;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import loggingtool.data.DefaultData;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.IOManager;
import loggingtool.ui.connect.send.SendJBtn;

public class SendActionAdapter implements ActionListener {
	
	private SendJBtn sendJBtn;
	
	public SendActionAdapter() {
		sendJBtn = IOManager.getSendJBtn();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		BlueToothConnection.send(getSendData());
		sendJBtn.changeSelectable(false);
	}

	public static String getSendData() {
		String delimiter = DefaultData.DELIMITER;
		String sendData = SendData.getFirstData() + delimiter +
						  SendData.getSecondData() + delimiter +
						  SendData.getThirdData() + delimiter +
						  SendData.getFourthData() + delimiter +
						  SendData.getFifthData();
		return sendData;
	}
}

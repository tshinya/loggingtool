package loggingtool.ui.chart;

import java.util.Iterator;
import loggingtool.data.receive.Time;
import org.jfree.chart.ChartColor;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public class NxtJChart {
	
	private ChartPanel chartPnl;
	
	public NxtJChart() {
		JFreeChart chart = ChartFactory.createScatterPlot("NXT - Chart", "Time", "Data", null, PlotOrientation.VERTICAL, false, true, false); 
		chart.setBackgroundPaint(ChartColor.WHITE);
		chartPnl = new ChartPanel(chart);
	}
	
	public NxtJChart(Iterator<Integer> dataList, String data) {
		IntervalXYDataset dataset = toIntervalXYDataset(dataList);
		JFreeChart chart = ChartFactory.createScatterPlot(data, "Time", data, dataset, PlotOrientation.VERTICAL, false, true, false);
		chart.setBackgroundPaint(ChartColor.WHITE);
		chartPnl = new ChartPanel(chart);
	}
	
	public ChartPanel getChartPnl() {
		return this.chartPnl;
	}
	
	private IntervalXYDataset toIntervalXYDataset(Iterator<Integer> dataList) {
		XYSeries data = new XYSeries("NXT");
		Iterator<Integer> time = Time.Iterator();
		while (time.hasNext() && dataList.hasNext()) {
			data.add(time.next(), dataList.next());
		}
		return new XYSeriesCollection(data);
	}

}
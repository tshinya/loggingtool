package loggingtool.ui.chart;

import java.awt.GridLayout;
import javax.swing.JPanel;
import java.util.ArrayList;
import java.util.Iterator;
import loggingtool.data.receive.Time;

public class ChartJPnl extends JPanel {
	
	public ChartJPnl() {
		this.setLayout(new GridLayout(1, 1));
		ArrayList<Integer> data = new ArrayList<>();
		for (int i = 0; i < 100; i++) {
			data.add(i);
			Time.add(i);
		}
		NxtJChart nxtJChart = new NxtJChart();
		this.add(nxtJChart.getChartPnl());
	}

	public void updateChart(Iterator<Integer> dataList, String data) {
		this.removeAll();
		NxtJChart nxtJChart = new NxtJChart(dataList, data);
		this.add(nxtJChart.getChartPnl());
		this.validate();
	}
}
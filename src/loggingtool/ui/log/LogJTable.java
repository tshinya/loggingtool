package loggingtool.ui.log;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import loggingtool.data.DefaultData;
import loggingtool.function.connection.BlueToothConnection;
import loggingtool.function.draw.DrawActionAdapter;

public class LogJTable extends JTable {
	
	private DefaultTableModel model;

	public LogJTable() {
		String[] tableData = DefaultData.NXT_DATA;
		model = new DefaultTableModel(tableData, 0);
		BlueToothConnection.setLogJTable(this);
		final JTableHeader header = this.getTableHeader();
		header.addMouseListener(new DrawActionAdapter(this));
		this.setEnabled(false);
		this.setAutoscrolls(true);
		this.setModel(model);
	}
	
	public void appendLog(String[] row) {
		if (model.getRowCount() > DefaultData.MAX_LINE) {
			model.removeRow(0);
		}
		model.addRow(row);
		//int rowCount = model.getRowCount();
		//this.scrollRectToVisible(new Rectangle(this.getCellRect(rowCount, 0, false)));
	}
}
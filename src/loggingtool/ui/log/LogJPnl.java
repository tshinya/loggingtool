package loggingtool.ui.log;

import java.awt.GridLayout;
import javax.swing.JPanel;

public class LogJPnl extends JPanel {

	public LogJPnl() {
		this.setLayout(new GridLayout(1, 1));
		LogJScrlPane logJScrlPane = new LogJScrlPane();
		this.add(logJScrlPane);
	}
}

package loggingtool.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.Iterator;

import javax.swing.JFrame;
import loggingtool.ui.chart.ChartJPnl;
import loggingtool.ui.connect.ConnectJPnl;
import loggingtool.ui.log.LogJPnl;

public class MainFrame extends JFrame {
	
	private static ChartJPnl chartJPnl;

	public MainFrame() {
		super("NXT-DataLogger");

		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 0.4;

		gbc.weightx = 0.3;
		ConnectJPnl connectJPanel = new ConnectJPnl();
		layout.setConstraints(connectJPanel, gbc);
		
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weightx = 0.7;
		chartJPnl = new ChartJPnl();
		layout.setConstraints(chartJPnl, gbc);
		
		gbc.weighty = 0.6;
		gbc.weightx = 1.0;
		LogJPnl logJPnl = new LogJPnl();
		layout.setConstraints(logJPnl, gbc);
		
		this.add(connectJPanel);
		this.add(chartJPnl);
		this.add(logJPnl);
		
		this.setSize(1000, 700);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setVisible(true);
	}
	
	public static void updateChart(Iterator<Integer> dataList, String data) {
		chartJPnl.updateChart(dataList, data);
	}
	
}
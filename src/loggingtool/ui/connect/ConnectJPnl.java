package loggingtool.ui.connect;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import javax.swing.JPanel;
import loggingtool.ui.connect.address.AddressJPnl;
import loggingtool.ui.connect.senddata.SendDataJPnl;

public class ConnectJPnl extends JPanel {

	public ConnectJPnl() {
		
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weighty = 0.1;
		AddressJPnl addressJPanel = new AddressJPnl();
		layout.setConstraints(addressJPanel, gbc);
		
		gbc.weighty = 0.8;
		SendDataJPnl sendValueJPanel = new SendDataJPnl();
		layout.setConstraints(sendValueJPanel, gbc);
		
		JPanel pnl = new JPanel();
		pnl.setLayout(new GridLayout(1, 2));
		pnl.add(IOManager.getSendJBtn());
		pnl.add(IOManager.getReceiveJBtn());
		
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		gbc.weighty = 0.1;
		layout.setConstraints(pnl, gbc);
		
		this.add(addressJPanel);
		this.add(sendValueJPanel);
		this.add(pnl);
	}
}
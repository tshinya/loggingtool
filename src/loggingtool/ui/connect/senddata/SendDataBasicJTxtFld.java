package loggingtool.ui.connect.senddata;

import javax.swing.JTextField;

public abstract class SendDataBasicJTxtFld extends JTextField {
	
	public abstract void toDefault();
	
	public boolean isSendData() {
		if (toSendData() >= 0) {
			return true;
		}
		return false;
	}
	
	public int toSendData() {
		try {
			return Integer.valueOf(this.getText());
		} catch (Exception e) {
			toDefault();
			return -1;
		}
	}

}
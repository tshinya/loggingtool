package loggingtool.ui.connect.senddata;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

import loggingtool.ui.connect.senddata.fifth.FifthValueJPnl;
import loggingtool.ui.connect.senddata.first.FirstValueJPnl;
import loggingtool.ui.connect.senddata.fourth.FourthValueJPnl;
import loggingtool.ui.connect.senddata.second.SecondValueJPnl;
import loggingtool.ui.connect.senddata.third.ThridValueJPnl;




public class SendDataJPnl extends JPanel {
	
	GridBagLayout layout;
	GridBagConstraints gbc;
	
	public SendDataJPnl() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		
		setValuePanel(new FirstValueJPnl());
		setValuePanel(new SecondValueJPnl());
		setValuePanel(new ThridValueJPnl());
		setValuePanel(new FourthValueJPnl());
		setValuePanel(new FifthValueJPnl());
	}
	
	private void setValuePanel(JPanel valueJPanel) {
		gbc.gridwidth = GridBagConstraints.REMAINDER;
		layout.setConstraints(valueJPanel, gbc);
		this.add(valueJPanel);
	}

}
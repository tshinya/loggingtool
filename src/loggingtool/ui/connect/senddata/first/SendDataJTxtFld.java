package loggingtool.ui.connect.senddata.first;

import loggingtool.data.send.ChangeFirstDataActionAdapter;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class SendDataJTxtFld extends SendDataBasicJTxtFld {

	SendDataJTxtFld() {
		ChangeFirstDataActionAdapter changeFirstDataActionAdapter = new ChangeFirstDataActionAdapter(this);
		this.addActionListener(changeFirstDataActionAdapter);
		this.addFocusListener(changeFirstDataActionAdapter);
		toDefault();
	}

	@Override
	public void toDefault() {
		int sendData = SendData.getFirstData();
		String defaultValue = String.valueOf(sendData);
		this.setText(defaultValue);
	}
}

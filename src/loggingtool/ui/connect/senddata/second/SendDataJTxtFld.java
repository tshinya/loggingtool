package loggingtool.ui.connect.senddata.second;

import loggingtool.data.send.ChangeSecondDataActionAdapter;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class SendDataJTxtFld extends SendDataBasicJTxtFld {

	SendDataJTxtFld() {
		ChangeSecondDataActionAdapter changeSecondDataActionAdapter = new ChangeSecondDataActionAdapter(this);
		this.addActionListener(changeSecondDataActionAdapter);
		this.addFocusListener(changeSecondDataActionAdapter);
		toDefault();
	}

	@Override
	public void toDefault() {
		int sendData = SendData.getSecondData();
		String defaultValue = String.valueOf(sendData);
		this.setText(defaultValue);
	}
}

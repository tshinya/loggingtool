package loggingtool.ui.connect.senddata.third;

import loggingtool.data.send.ChangeThirdDataActionAdapter;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class SendDataJTxtFld extends SendDataBasicJTxtFld {

	SendDataJTxtFld() {
		ChangeThirdDataActionAdapter changeThirdDataActionAdapter = new ChangeThirdDataActionAdapter(this);
		this.addActionListener(changeThirdDataActionAdapter);
		this.addFocusListener(changeThirdDataActionAdapter);
		toDefault();
	}

	@Override
	public void toDefault() {
		int sendData = SendData.getThirdData();
		String defaultValue = String.valueOf(sendData);
		this.setText(defaultValue);
	}
}

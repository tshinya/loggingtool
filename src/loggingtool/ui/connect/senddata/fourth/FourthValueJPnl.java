package loggingtool.ui.connect.senddata.fourth;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JPanel;

public class FourthValueJPnl extends JPanel {

	public FourthValueJPnl() {
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		
		gbc.weighty = 0.3;
		ValueJLabel valueJLabel = new ValueJLabel();
		layout.setConstraints(valueJLabel, gbc);
		
		gbc.weightx = 0.7;
		SendDataJTxtFld sendDataJTxtFld = new SendDataJTxtFld();
		layout.setConstraints(sendDataJTxtFld, gbc);
		
		this.add(valueJLabel);
		this.add(sendDataJTxtFld);
	}
}

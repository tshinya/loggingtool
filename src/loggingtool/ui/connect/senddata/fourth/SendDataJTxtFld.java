package loggingtool.ui.connect.senddata.fourth;

import loggingtool.data.send.ChangeFourthDataActionAdapter;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class SendDataJTxtFld extends SendDataBasicJTxtFld {

	SendDataJTxtFld() {
		ChangeFourthDataActionAdapter changeFourthDataActionAdapter = new ChangeFourthDataActionAdapter(this);
		this.addActionListener(changeFourthDataActionAdapter);
		this.addFocusListener(changeFourthDataActionAdapter);
		toDefault();
	}

	@Override
	public void toDefault() {
		int sendData = SendData.getFourthData();
		String defaultValue = String.valueOf(sendData);
		this.setText(defaultValue);
	}
}

package loggingtool.ui.connect.senddata.fifth;

import loggingtool.data.send.ChangeFifthDataActionAdapter;
import loggingtool.data.send.SendData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class SendDataJTxtFld extends SendDataBasicJTxtFld {

	SendDataJTxtFld() {
		ChangeFifthDataActionAdapter changeFifthDataActionAdapter = new ChangeFifthDataActionAdapter(this);
		this.addActionListener(changeFifthDataActionAdapter);
		this.addFocusListener(changeFifthDataActionAdapter);
		toDefault();
	}

	@Override
	public void toDefault() {
		int sendData = SendData.getFifthData();
		String defaultValue = String.valueOf(sendData);
		this.setText(defaultValue);
	}
}
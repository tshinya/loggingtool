package loggingtool.ui.connect.address;

import javax.swing.JButton;

import loggingtool.function.connection.ConnectActionAdapter;

public class ConnectJBtn extends JButton {
	
	public ConnectJBtn() {
		disConnect();
		this.addActionListener(new ConnectActionAdapter(this));
	}

	public void disConnect() {
		this.setText("Connect");
	}
	
	public void connect() {
		this.setText("  Close  ");
	}
}
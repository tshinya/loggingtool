package loggingtool.ui.connect.address;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

public class AddressJPnl extends JPanel {

	public AddressJPnl() {
		GridBagLayout layout = new GridBagLayout();
		this.setLayout(layout);
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.fill = GridBagConstraints.BOTH;
		gbc.weighty = 1.0;
		
		gbc.weightx = 0.7;
		AddressJTxtFld addressJTextField = new AddressJTxtFld();
		layout.setConstraints(addressJTextField, gbc);
		
		gbc.weightx = 0.3;
		ConnectJBtn connectJButton = new ConnectJBtn();
		layout.setConstraints(connectJButton, gbc);
		
		this.add(addressJTextField);
		this.add(connectJButton);
	}
}

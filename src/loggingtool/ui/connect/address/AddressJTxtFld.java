package loggingtool.ui.connect.address;

import javax.swing.JTextField;

import loggingtool.data.DefaultData;
import loggingtool.data.address.ChangeAddressActionAdapter;



public class AddressJTxtFld extends JTextField {
	
	public AddressJTxtFld() {
		super(DefaultData.ADDRESS);
		this.addActionListener(new ChangeAddressActionAdapter(this));
	}

}

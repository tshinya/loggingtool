package loggingtool.ui.connect.receive;

import javax.swing.JButton;
import loggingtool.function.connection.BlueToothConnection;
import loggingtool.function.connection.ReceiveActionAdapter;

public class ReceiveJBtn extends JButton {

	public ReceiveJBtn() {
		super("Receive");
		this.addActionListener(new ReceiveActionAdapter());
		changeSelectable(BlueToothConnection.isConnect());
	}
	
	public void changeSelectable(boolean flag) {
		this.setEnabled(flag);
	}
}
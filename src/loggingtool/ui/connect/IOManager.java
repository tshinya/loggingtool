package loggingtool.ui.connect;

import loggingtool.ui.connect.receive.ReceiveJBtn;
import loggingtool.ui.connect.send.SendJBtn;

public class IOManager {
	
	private static SendJBtn sendJBtn = new SendJBtn();
	private static ReceiveJBtn receiveJBtn = new ReceiveJBtn();
	
	public static SendJBtn getSendJBtn() {
		return sendJBtn;
	}
	
	public static ReceiveJBtn getReceiveJBtn() {
		return receiveJBtn;
	}

}
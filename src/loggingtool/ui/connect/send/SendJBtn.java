package loggingtool.ui.connect.send;

import javax.swing.JButton;
import loggingtool.function.connection.BlueToothConnection;
import loggingtool.function.connection.SendActionAdapter;

public class SendJBtn extends JButton {

	public SendJBtn() {
		super("Send");
		this.addActionListener(new SendActionAdapter());
		changeSelectable(BlueToothConnection.isConnect());
	}
	
	public void changeSelectable(boolean flag) {
		this.setEnabled(flag);
	}
}
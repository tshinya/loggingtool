package loggingtool.data.send;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import loggingtool.data.DefaultData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class ChangeSecondDataActionAdapter implements ActionListener, FocusListener {
	
	private SendDataBasicJTxtFld sendDataBasicJTxtFld;
	
	public ChangeSecondDataActionAdapter(SendDataBasicJTxtFld _sendDataBasicJTxtFld) {
		sendDataBasicJTxtFld = _sendDataBasicJTxtFld;
		SendData.changeSecondData(DefaultData.SEND_DATA);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		change();
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void focusLost(FocusEvent e) {
		change();
	}
	
	private void change() {
		if (sendDataBasicJTxtFld.isSendData()) {
			SendData.changeSecondData(sendDataBasicJTxtFld.toSendData());
			return ;
		}
		sendDataBasicJTxtFld.toDefault();
	}

}
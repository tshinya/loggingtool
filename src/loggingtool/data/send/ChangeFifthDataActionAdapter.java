package loggingtool.data.send;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import loggingtool.data.DefaultData;
import loggingtool.ui.connect.senddata.SendDataBasicJTxtFld;

public class ChangeFifthDataActionAdapter implements ActionListener, FocusListener {
	
	private SendDataBasicJTxtFld sendDataBasicJTxtFld;
	
	public ChangeFifthDataActionAdapter(SendDataBasicJTxtFld _sendDataBasicJTxtFld) {
		sendDataBasicJTxtFld = _sendDataBasicJTxtFld;
		SendData.changeFifthData(DefaultData.SEND_DATA);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		change();
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void focusLost(FocusEvent e) {
		change();
	}

	void change() {
		if (sendDataBasicJTxtFld.isSendData()) {
			SendData.changeFifthData(sendDataBasicJTxtFld.toSendData());
			return ;
		}
		sendDataBasicJTxtFld.toDefault();
	}
}
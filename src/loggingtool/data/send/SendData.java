package loggingtool.data.send;

public class SendData {
	
	private static int firstData;
	private static int secondData;
	private static int thirdData;
	private static int fourthData;
	private static int fifthData;
	
	private SendData() {
		
	}
	
	static void changeFirstData(int _firstData) {
		firstData = _firstData;
	}
	
	static void changeSecondData(int _secondData) {
		secondData = _secondData;
	}
	
	static void changeThirdData(int _thirdData) {
		thirdData = _thirdData;
	}
	
	static void changeFourthData(int _fourthData) {
		fourthData = _fourthData;
	}

	static void changeFifthData(int _fifthData) {
		fifthData = _fifthData;
	}
	
	public static int getFirstData() {
		return firstData;
	}
	
	public static int getSecondData() {
		return secondData;
	}
	
	public static int getThirdData() { 
		return thirdData;
	}
	
	public static int getFourthData() {
		return fourthData;
	}
	
	public static int getFifthData() {
		return fifthData;
	}
}
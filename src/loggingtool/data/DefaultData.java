package loggingtool.data;

public class DefaultData {
	
	public final static int SEND_DATA = 0;
	public final static String NO_DATA = "////";
	public final static String ADDRESS = "btspp://0016530c7bb9:1;authenticate=false;encrypt=false;master=false";
	public final static String DELIMITER = "/";
	public final static int MAX_LINE = 1000;
	
	public final static String TIME = "Time";
	public final static String S8_1 = "S8_1";
	public final static String S8_2 = "S8_2";
	public final static String LIGHT = "Light";
	public final static String MOTOR_A = "MotorA";
	public final static String MOTOR_B = "MotorB";
	public final static String MOTOR_C = "MotorC";
	public final static String S16_1 = "S16_1";
	public final static String S16_2 = "S16_2";
	public final static String S16_3 = "S16_3";
	public final static String S16_4 = "S16_4";
	public final static String WAVE = "Wave";
	
	public final static String[] NXT_DATA = {TIME, S8_1, S8_2, LIGHT, MOTOR_A, MOTOR_B, MOTOR_C, S16_1, S16_2, S16_3, S16_4, WAVE};

	private DefaultData() {
		
	}

}
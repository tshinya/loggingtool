package loggingtool.data.address;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;

import loggingtool.ui.connect.address.AddressJTxtFld;

public class ChangeAddressActionAdapter implements ActionListener, FocusListener {

	private AddressJTxtFld addressJTxtFld;
	
	public ChangeAddressActionAdapter(AddressJTxtFld _addressJTxtFld) {
		addressJTxtFld = _addressJTxtFld;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		change();
	}

	@Override
	public void focusGained(FocusEvent e) {
		// TODO Auto-generated method stub
	}

	@Override
	public void focusLost(FocusEvent e) {
		change();
	}
	
	private void change() {
		String address = addressJTxtFld.getText();
		Address.change(address);
	}

}
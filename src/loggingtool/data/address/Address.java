package loggingtool.data.address;

import loggingtool.data.DefaultData;

public class Address {
	
	private static String address = DefaultData.ADDRESS;
	
	private Address() {
		
	}
	
	static void change(String _address) {
		address = _address;
	}
	
	public static String get() {
		return address;
	}
}
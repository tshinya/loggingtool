package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class Light {

	private static ArrayList<Integer> light = new ArrayList<>();

	private Light() {
		
	}

	public static void add(int value) {
		light.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return light.iterator();
	}
	
	public static void clear() {
		light.clear();
	}
	
}
package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;

public class MotorC {

	private static ArrayList<Integer> motorC = new ArrayList<>();

	private MotorC() {
		
	}

	public static void add(int value) {
		motorC.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return motorC.iterator();
	}
	
	public static void clear() {
		motorC.clear();
	}
}

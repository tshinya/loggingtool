package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class S16_2 {

	private static ArrayList<Integer> s16_2 = new ArrayList<>();

	private S16_2() {
		
	}

	public static void add(int value) {
		s16_2.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s16_2.iterator();
	}
	
	public static void clear() {
		s16_2.clear();
	}
}

package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class S16_1 {

	private static ArrayList<Integer> s16_1 = new ArrayList<>();

	private S16_1() {
		
	}

	public static void add(int value) {
		s16_1.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s16_1.iterator();
	}
	
	public static void clear() {
		s16_1.clear();
	}
}

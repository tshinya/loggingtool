package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;

public class MotorB {
	
	private static ArrayList<Integer> motorB = new ArrayList<>();

	private MotorB() {
		
	}

	public static void add(int value) {
		motorB.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return motorB.iterator();
	}
	
	public static void clear() {
		motorB.clear();
	}
}

package loggingtool.data.receive;

public class Convertor {
	
	private Convertor() {
		
	}
	
	public static int toUInt16(byte[] buf, int index) {
		int b[] = new int[2];
		b[0] = Integer.valueOf(buf[index]) & 0xFF;
		b[1] = Integer.valueOf(index) & 0xFF;
		return b[0] + b[1]*256;
	}
	
	public static int toUInt32(byte[] buf, int index) {
		int[] b = new int[4];
		b[0] = Integer.valueOf(buf[index]) & 0xFF;
		b[1] = Integer.valueOf(buf[index+1]) & 0xFF;
		b[2] = Integer.valueOf(buf[index+2]) & 0xFF;
		b[3] = Integer.valueOf(buf[index+3]) & 0xFF;
		return b[0] + b[1]*256 + b[2]*256*256 + b[3]*256*256*256;
	}
	
	public static int toInt16(byte[] buf, int index) {
		int b[] = new int[2];
		b[0] = Integer.valueOf(buf[index]);
		b[1] = Integer.valueOf(buf[index+1]);
		if (b[1] < 128) {
			b[0] = b[0] & 0xFF;
		}
		return b[0] + b[1]*256;
	}
	
	public static int toInt32(byte[] buf, int index) {
		int[] b = new int[4];
		b[0] = Integer.valueOf(buf[index]);
		b[1] = Integer.valueOf(buf[index+1]);
		b[2] = Integer.valueOf(buf[index+2]);
		b[3] = Integer.valueOf(buf[index+3]);
		if(b[3] < 128) {
			b[2] = b[2] & 0xFF;
			b[1] = b[1] & 0xFF;
			b[0] = b[0] & 0xFF;
		}
		return b[0] + b[1]*256 + b[2]*256*256 + b[3]*256*256*256; 
	}

}
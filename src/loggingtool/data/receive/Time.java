package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;

public class Time {
	private static ArrayList<Integer> time = new ArrayList<>();
	private static int start;
	
	private Time() {
		
	}
	
	public static void add(int value) {
		if (time.size() == 0) {
			start = value;
		}
		value -= start;
		time.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return time.iterator();
	}
	
	public static void clear() {
		time.clear();
	}
}

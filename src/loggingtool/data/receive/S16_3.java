package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class S16_3 {

	private static ArrayList<Integer> s16_3 = new ArrayList<>();

	private S16_3() {
	
	}

	public static void add(int value) {
		s16_3.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s16_3.iterator();
	}
	
	public static void clear() {
		s16_3.clear();
	}
}

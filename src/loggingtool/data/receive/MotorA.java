package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;

public class MotorA {

	private static ArrayList<Integer> motorA = new ArrayList<>();
	
	private MotorA() {
		
	}
	
	public static void add(int value){
		motorA.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return motorA.iterator();
	}
	
	public static void clear() {
		motorA.clear();
	}
}

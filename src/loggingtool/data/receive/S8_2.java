package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class S8_2 {

	private static ArrayList<Integer> s8_2 = new ArrayList<>();

	private S8_2() {
		
	}
	
	public static void add(int value) {
		s8_2.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s8_2.iterator();
	}
	
	public static void clear() {
		s8_2.clear();
	}
}
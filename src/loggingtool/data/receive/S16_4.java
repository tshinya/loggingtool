package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class S16_4 {

	private static ArrayList<Integer> s16_4 = new ArrayList<>();

	private S16_4() {
		
	}

	public static void add(int value) {
		s16_4.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s16_4.iterator();
	}
	
	public static void clear() {
		s16_4.clear();
	}
}

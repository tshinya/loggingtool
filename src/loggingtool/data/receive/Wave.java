package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;


public class Wave {

	private static ArrayList<Integer> wave = new ArrayList<>();

	private Wave() {
		
	}

	public static void add(int value) {
		wave.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return wave.iterator();
	}
	
	public static void clear() {
		wave.clear();
	}
}

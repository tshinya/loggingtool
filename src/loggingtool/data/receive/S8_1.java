package loggingtool.data.receive;

import java.util.ArrayList;
import java.util.Iterator;

public class S8_1 {
	private static ArrayList<Integer> s8_1 = new ArrayList<>();

	private S8_1() {
		
	}
	
	public static void add(int value) {
		s8_1.add(value);
	}
	
	public static Iterator<Integer> Iterator() {
		return s8_1.iterator();
	}
	
	public static void clear() {
		s8_1.clear();
	}
}
